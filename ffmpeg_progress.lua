require ('iuplua')

local cancelflag		
local downloadProgress		

local function downloadStart(YtLink)
  cancelButton = iup.button{
    title = "Cancel",
    action = function()	      
      cancelflag = true         
      end

  }
  
  local downloadProgress = iup.progressbar{expand="HORIZONTAL", MARQUEE="YES"}	
  local dlgProgress = iup.dialog{				   
  title = "converting",
  dialogframe = "YES", border = "YES",
  iup.vbox {			
    downloadProgress,
    cancelButton,
    }
  }
  dlgProgress.size = "QUARTERxEIGHT" 		
  dlgProgress.menubox = "NO"		
  dlgProgress.close_cb = cancelButton.action	
  dlgProgress:showxy(iup.CENTER, iup.CENTER)	
  return dlgProgress
end


local function DL_progress(YtLink) 
  downloadStart() 		  
  local pipe = io.popen(YtLink)
  
  repeat
    local c = pipe:read()	  
    if c then 		
      iup.Flush()		     
      iup.LoopStep() 
      iup.Flush() 
      if cancelflag then  
       iup.Message("Terminated", "You have canceled the conversion") 
       iup.Flush()
       pipe:close()
       iup.Flush()
       return false 
      end
    end
  until not c
  pipe:close() 
  iup.Close()
  return true
end

convert_mp3 = {}
convert_mp3.go = function(tubeLink)
local flag =  DL_progress(tubeLink)
if not flag then return false else return true end
end


