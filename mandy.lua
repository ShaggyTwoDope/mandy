#!/usr/bin/lua5.2

require("iuplua")
require("iupluacontrols")
require('progress')
require('ffmpeg_progress')

--Main url entry box
function url_entry()
  local url = ""
  local format = 0
  res, url, format = iup.GetParam("Mandy 0.5.0 - Enter URL", nil,
                          "Enter Youtube URL: %s\n"..
                          "Select an audio format: %l|mp3|ogg|flac|\n", url, format)

  gFormat = sel_Codec(format)
  if res == true then
    if string.match(url, "youtube.com/watch") then
      return url
    else
      iup.Message("Error", "The URL is not a youtube link")
      iup.Flush()
      iup.Close()
    end
  else
    return false
  end
end


--make format dropdown menu items work
function sel_Codec(x)
  if x == 0 then
    local mp3 = ".mp3"
    print("Selected "..mp3)
    return mp3
  elseif x == 1 then
    local ogg = ".ogg"
    print("Selected "..ogg)
    return ogg
  elseif x == 2 then
    local flac = ".flac"
    print("Selected "..flac)
    return flac
  end

end

--Ask user to set directory
function set_dir()

  getPath = iup.filedlg{dialogtype = "SAVE", title="Save file...",
                        filter="*.mp3; *.ogg; *.flac",
                        directory=".",
                        file=""}

  getPath:popup(iup.ANYWHERE, iup.ANYWHERE)

  local status = getPath.status

  if status == "1" or status == "0"  then
    if string.match(getPath.value, "%"..gFormat) then
      local savedPath = getPath.value
      return savedPath

    else
      local savedPath = getPath.value..gFormat
      return savedPath

    end
    
  elseif status == "-1" then
    return false
  end
end

function ytDl(x)
  local tmpName = math.random(0,999999999999)
  local tmpNameTwo = math.random(0,999999999999)
  local tmpPath = x.." --output=/tmp/youtube-dl-"..tmpName.."-"..tmpNameTwo..".flv"
  local tmpPath2 = "/tmp/youtube-dl-"..tmpName.."-"..tmpNameTwo..".flv"

  local flag = popin.ytdl(tmpPath)
  if not flag then return false end
  
  return tmpPath2
end

--optional function to implement autoplay
function ask_play(x)
  play = ""
  res, play = iup.GetParam("Play now?", nil,
                          "Do you want to play the file now? %t\n", play)

  if res == true then
    io.popen("notify-send 'The file was saved to' "..x)
    io.popen("xdg-open "..x)
  else
    return false
  end

end


function ffmpeg(tmpPath, dirD)

  local subSpaces = string.gsub(dirD, " ", "\\ ")
  local mp3 = "ffmpeg -i "..tmpPath.." -acodec libmp3lame -ac 2 -ab 192k -vn -y "..subSpaces
  local ogg = "ffmpeg -i "..tmpPath.." -acodec libvorbis -ac 2 -ab 192k -vn -y "..subSpaces
  local flac = "ffmpeg -i "..tmpPath.." -acodec flac -ac 2 -ab 192k -vn -y "..subSpaces
  
  if gFormat == ".mp3" then
  local flag = convert_mp3.go(mp3)
  elseif gFormat == ".ogg" then
    local flag = convert_mp3.go(ogg)
  elseif gFormat == ".flac" then
   local flag = convert_mp3.go(flac)
  else
    if not flag then return false end

  end
  io.popen("notify-send 'The file was saved to' "..subSpaces)
end

--put everything into a table to use local functions globally
global = {}
global.url = function ()
  local url = url_entry()
  return url
end

global.dir = function()
  local dir = set_dir()
  return dir
end

global.path = function(url)
  local yt = ytDl(url)
  return yt
end

global.ffmpeg = function(tmpPath, saveDir)
  local convert = ffmpeg(tmpPath, saveDir)
  return convert
end


