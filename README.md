#Mandy - current version: 0.5.0


##About

Mandy is a script written in [Lua][1] using the [IUP][2] GUI toolkit. The
purpose of Mandy is to provide a simple interface for downloading
youtube videos and converting them to different audio formats.
Currently only mp3 format is supported.

Mandy currently relies on youtube-dl and ffmpeg for video downloading and
converting however, if native libraries surface in the future the
current back-end will be replaced.

Current platforms: Linux(x86/x64)

##Screenshots


![url entry][3]

![progressbar][4]

![converting][8]


##Installation

In order to run Mandy you will need to download and install it's
dependancies.


- lua 5.2
- libfreetype6-dev (ubuntu)
- youtube-dl
- ffmpeg
- libnotify - push notifications
- git
- [IUP][5]
- [CD][6]
- [IM][7] 


Since the IUP libraries are not specific to one distribution, we will
cover it's installation separately. 


###Arch Linux

For Arch Linux we start with getting the dependencies already in the
main repository:



    pacman -S lua youtube-dl ffmpeg git libnotify 



###Ubuntu

Ubuntu users will need to get an extra package in order to do the
conversion to mp3:



    sudo apt-get install lua5.2 youtube-dl ffmpeg vlc git libavcodec-extra-53 libfreetype6-dev libnotify


###IUP

IUP is a little more involved than the previous steps. For IUP we need to download each related library separately and edit a script before we run it and install the libraries. Since the documentation is difficult to find for this step, we will cover it here.

We will be getting three tar.gz files starting with the main one, IUP. We will also need the library packages "CD" and "IM" which IUP uses for extra GUI features. The steps for all three are the same so I will only cover them once.

NOTE: These instructions are for Linux Only!

Download the compressed tar.gz file:

32-bit


    # wget http://iweb.dl.sourceforge.net/project/iup/3.8/Linux%20Libraries/iup-3.8_Linux32_lib.tar.gz

64-bit

    # wget http://hivelocity.dl.sourceforge.net/project/iup/3.8/Linux%20Libraries/iup-3.8_Linux32_64_lib.tar.gz
    
Since this tar.gz package does not include a directory we will need to create one:

    # mkdir iup
    
Next we will extract the tar.gz file into the directory we just created:

    # tar xvf iup-X.X_LinuXX_XX_lib.tar.gz -C iup

Now change to that directory:

    # cd iup/
    
Before we can run the install scripts wil need to do a few things first.

First we need to edit the lua module file: 

    # nano config_lua_module

and change the top two lines to correspond to your version of lua which in our case is 5.2 at the time of this writing:

Change this:

    #Current Lua Version
    LUA_VER=5.1
    LUA_PFX=51
    
Should look like this:

    #Current Lua Version
    LUA_VER=5.2
    LUA_PFX=52
    
After you have saved the lua module file, the next thing to do is to make the remaining install scripts executables:

    # chmod +x install install_dev config_lua_module
    
Now we are ready to run all three scripts. These scripts will install the library files needed for IUP to be used with Lua. Since these libraries will copied to the /lib directory, you will need to run all three install scrips as root:

    # ./install
    # ./install_dev
    # ./config_lua_module
    
The IUP libraries should now be properly installed on your system. This will give Mandy basic graphical capabilities but will not provide us with all the features Mandy needs to run.

We still need to get the other two dependencies: CD and IM.

####CD

Download the compressed tar.gz file:

32-bit

    # wget http://iweb.dl.sourceforge.net/project/canvasdraw/5.6.1/Linux%20Libraries/cd-5.6.1_Linux32_lib.tar.gz
    
64-bit

    # wget http://iweb.dl.sourceforge.net/project/canvasdraw/5.6.1/Linux%20Libraries/cd-5.6.1_Linux32_64_lib.tar.gz
    
Create a directory to extract to:

    # mkdir cd
    
Extract the file:

    # tar xvf cd-X.X.X_LinuxXX_lib.tar.gz -C cd
    
Change directories:

    # cd cd
    
Now all you have to do is re-follow the steps listed above in the "IUP" section and you're ready to move on to installing IM.

####IM

Download the tar.gz file:

32-bit

    # wget http://superb-dca2.dl.sourceforge.net/project/imtoolkit/3.8.1/Linux%20Libraries/im-3.8.1_Linux32_lib.tar.gz

64-bit

    # wget http://superb-dca2.dl.sourceforge.net/project/imtoolkit/3.8.1/Linux%20Libraries/im-3.8.1_Linux32_64_lib.tar.gz
    
Create a new directory:

    # mkdir im
    
Extract the file:

    # tar xvf im-3.8.1_Linux32_lib.tar.gz -C im
    
Now just follow the steps in the IUP section above as before and run the scripts and you're finally done.


##Getting Mandy


Now we can get the actual source files of Mandy. I recommend setting up a dedicated directory before doing this.

    # mkdir projects && cd projects
    
Now we can clone the repository into the projects folder:

    # git clone https://silvernode1@bitbucket.org/silvernode1/mandy.git
    # cd mandy
    
Now you are ready to run Mandy and see what it's all about. For Arch Linux users all you need to do is run lua:

    # lua main.lua
    
Ubuntu users need to run lua5.2 although optionally you can set up an alias and append it to your ~/.bashrc file:

    # alias lua5.2="lua"
    # lua main.lua
    

##Notes

In the future we hope to figure out how to include IUP, CD and IM as dynamic libraries in order to include them in a package with Mandy for various distributions i.e. Ubuntu derivatives. On the plus side (at least for Arch Linux users) in the meantime we plan to put together a few PKGBUILD for the Arch User Repository (AUR) which is a lot easier and will automate the installation process all the same. 

We hope you find Mandy useful and convenient. If you would like to contribute please send us a message through bitbucket. This is our first project so we could use some pointers on Lua in general as well as ideas for the project. 

Thanks!


    

[1]: http://lua.org
[2]: http://www.tecgraf.puc-rio.br/iup/
[3]: http://i.imgur.com/P2rMTiM.png
[4]: http://i.imgur.com/1AcLZk6.png
[5]: http://sourceforge.net/projects/iup/files/
[6]: http://sourceforge.net/projects/canvasdraw/files/
[7]: http://sourceforge.net/projects/imtoolkit/files/
[8]: http://i.imgur.com/XrmwZrX.png



 

